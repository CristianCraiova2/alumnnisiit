<?php

namespace App\Controller;

use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default/{name}", name="default")
     */
    public function index($name): Response
    {
        return $this->render('default/index.html.twig', [
            'name' => ucfirst($name),
        ]);
    }

    /**
     * @Route("/allCourses", name="default")
     */
    public function all(CourseRepository $courseRepository): Response
    {
        return $this->render('default/allCourses.html.twig', [
            'courses' => $courseRepository->findAll()
        ]);
    }
}
