<?php

namespace App\Controller;

use App\Entity\CourseGroup;
use App\Form\CourseGroupType;
use App\Repository\CourseGroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/grupa")
 */
class CourseGroupController extends AbstractController
{
    /**
     * @Route("/", name="course_group_index", methods={"GET"})
     */
    public function index(CourseGroupRepository $courseGroupRepository): Response
    {
        return $this->render('course_group/index.html.twig', [
            'course_groups' => $courseGroupRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="course_group_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $courseGroup = new CourseGroup();
        $form = $this->createForm(CourseGroupType::class, $courseGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($courseGroup);
            $entityManager->flush();

            return $this->redirectToRoute('course_group_index');
        }

        return $this->render('course_group/new.html.twig', [
            'course_group' => $courseGroup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="course_group_show", methods={"GET"})
     */
    public function show(CourseGroup $courseGroup): Response
    {
        return $this->render('course_group/show.html.twig', [
            'course_group' => $courseGroup,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="course_group_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CourseGroup $courseGroup): Response
    {
        $form = $this->createForm(CourseGroupType::class, $courseGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('course_group_index');
        }

        return $this->render('course_group/edit.html.twig', [
            'course_group' => $courseGroup,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="course_group_delete", methods={"POST"})
     */
    public function delete(Request $request, CourseGroup $courseGroup): Response
    {
        if ($this->isCsrfTokenValid('delete'.$courseGroup->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($courseGroup);
            $entityManager->flush();
        }

        return $this->redirectToRoute('course_group_index');
    }
}
