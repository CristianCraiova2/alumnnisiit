<?php

namespace App\Entity;

use App\Repository\ProgramsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgramsRepository::class)
 */
class Programs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ProgramsName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProgramsName(): ?string
    {
        return $this->ProgramsName;
    }

    public function setProgramsName(string $ProgramsName): self
    {
        $this->ProgramsName = $ProgramsName;

        return $this;
    }
}
