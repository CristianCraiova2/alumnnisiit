<?php

namespace App\Entity;

use App\Repository\CourseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CourseRepository::class)
 */
class Course
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=CourseGroup::class, mappedBy="course")
     */
    private $courseGroups;

    public function __construct()
    {
        $this->courseGroups = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|CourseGroup[]
     */
    public function getCourseGroups(): Collection
    {
        return $this->courseGroups;
    }

    public function addCourseGroup(CourseGroup $courseGroup): self
    {
        if (!$this->courseGroups->contains($courseGroup)) {
            $this->courseGroups[] = $courseGroup;
            $courseGroup->setCourse($this);
        }

        return $this;
    }

    public function removeCourseGroup(CourseGroup $courseGroup): self
    {
        if ($this->courseGroups->removeElement($courseGroup)) {
            // set the owning side to null (unless already changed)
            if ($courseGroup->getCourse() === $this) {
                $courseGroup->setCourse(null);
            }
        }

        return $this;
    }
}
