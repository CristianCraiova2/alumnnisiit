<?php

namespace App\Repository;

use App\Entity\CourseGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CourseGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method CourseGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method CourseGroup[]    findAll()
 * @method CourseGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CourseGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CourseGroup::class);
    }

    // /**
    //  * @return CourseGroup[] Returns an array of CourseGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CourseGroup
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
